
import express from 'express';

const fs = require('fs');
const errorHandler = require('errorhandler');
const morgan = require("morgan");
const app = express();
const rateLimit = require("express-rate-limit");
const bodyParser = require('body-parser');
const port = 3040;

const class_data = JSON.parse(fs.readFileSync('./includes/classes.json'));
const classes = new Array<String>();
const classIdMapping: any = {}
class_data.forEach((clazz: any) => {
    classIdMapping[clazz.number] = clazz;
    classes.push(clazz.number);
});
const class_names = classes.join("\n")

app.use(morgan(':date ":method :url" :status :res[content-length] - :response-time ms'));

morgan.token('date', function() {
    var p = new Date().toString().replace(/[A-Z]{3}\+/,'+').split(/ /);
    return( p[2]+'/'+p[1]+'/'+p[3]+':'+p[4]+' '+p[5] );
});

process.on('uncaughtException', function (exception) {
    console.log(exception);
});

process.on('unhandledRejection', (reason, p) => {
    console.log("Unhandled Rejection at: Promise ", p, " reason: ", reason);
});

app.use(errorHandler({ dumpExceptions: true, showStack: true })); 

// JSON Body Parser Configuration
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Request Throttler
app.set('trust proxy', 1);
const limiter = rateLimit({
  windowMs: 30 * 1000, // 30 seconds
  max: 5000 // limit each IP to 5000 requests per windowMs (30 second)
});
app.use(limiter);

// Allow CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/all_data', (req, res) => {
    res.send(class_data);
});

app.get('/classes/:classId', (req, res) => {
    const classId = req.params.classId
    if (classId in classIdMapping) {
        res.send(classIdMapping[classId]);
    } else {
        let datetime: Date = new Date();
        let datetimeStr: string = `${datetime.toLocaleDateString()} ${datetime.toLocaleTimeString()}`;
        res.status(404).send({
            "error-msg": `Class '${classId}' not found.`,
            "date-time": datetimeStr
        })
    }
});

app.get('/classes', (req, res) => {
    res.send(classes);
});

app.get('/classes_as_json', (req, res) => {
    res.send(classes);
});

app.get('/classes_as_txt', (req, res) => {
    res.send(class_names);
});


// Error Handling
app.use((err: any, req: any, res: any, next: any) => {
    let datetime: Date = new Date();
    let datetimeStr: string = `${datetime.toLocaleDateString()} ${datetime.toLocaleTimeString()}`;
    console.log(`${datetimeStr}: Encountered an error processing ${JSON.stringify(req.body)}`);
    res.status(400).send({
        "error-msg": "Oops! Something went wrong. Check to make sure that you are sending a valid request.",
        "date-time": datetimeStr
    })
});

// Open Server for Business
app.listen(port, () => {
    console.log(`CS220 Webserver listening on :${port}`)
});